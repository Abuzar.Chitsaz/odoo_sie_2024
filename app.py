from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_caching import Cache
from flask_mail import Mail, Message
import odoorpc #le module le plus pythonique pour interagir avec l'API d'Odoo
from datetime import datetime, timedelta
import pytz

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

# Paramètres de connexion
url = 'localhost'  
db = 'SIE_2024'
username = 'abu@cui.ch'
password = '100'

# Connexion au serveur Odoo
odoo = odoorpc.ODOO(url, port=8069)  
odoo.login(db, username, password)

# Configuration du fuseau horaire
local_tz = pytz.timezone('Europe/Paris')

# Connexion au fake SMTP https://ethereal.email/ (créer un compte et mettre les identifiants fournis par le site) 
app.config['MAIL_SERVER'] = 'smtp.ethereal.email'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = 'reina.ebert90@ethereal.email'
app.config['MAIL_PASSWORD'] = 'D3jJVQgBJbA5CCAe2U'
mail = Mail(app)

@app.route('/api/events')
@cache.cached(timeout=50) # Cache les résultats pour 50 secondes
def events():
    appointment_model = odoo.env['calendar.event']
    appointments = appointment_model.search_read([], ['start', 'stop'])
    events = []
    for appt in appointments:
        start_utc = pytz.utc.localize(datetime.strptime(appt['start'], '%Y-%m-%d %H:%M:%S'))
        end_utc = pytz.utc.localize(datetime.strptime(appt['stop'], '%Y-%m-%d %H:%M:%S'))
        # Convertir de UTC (Odoo stocke les temps en UTC) à votre fuseau horaire local
        start_local = start_utc.astimezone(local_tz)
        end_local = end_utc.astimezone(local_tz)
        events.append({
            'title': 'Reservée',
            'start': start_local.strftime('%Y-%m-%d %H:%M:%S'),
            'end': end_local.strftime('%Y-%m-%d %H:%M:%S')
        })
    return jsonify(events)

def find_or_create_contact(email, name):
    partner_model = odoo.env['res.partner']
    partner = partner_model.search([('email', '=', email)])
    if not partner:
        partner_id = partner_model.create({'name': name, 'email': email})
    else:
        partner_id = partner[0]
    return partner_id

#récupérerez les données du formulaire
@app.route('/submit', methods=['POST'])
def submit():
    name = request.form['name']
    email = request.form['email']
    date = request.form['date']
    time = request.form['time']

    # Création ou recherche d'un contact
    partner_id = find_or_create_contact(email, name)

    # Configuration du fuseau horaire et des dates
    start_datetime = local_tz.localize(datetime.strptime(f"{date} {time}", '%Y-%m-%d %H:%M'))
    start_datetime = start_datetime - timedelta(hours=2)  # Ajustement nécessaire pour l'heure d'été/hiver
    end_datetime = start_datetime + timedelta(hours=1)
    
    # Vérification de la disponibilité du créneau
    appointment_model = odoo.env['calendar.event']
    existing_appointments = appointment_model.search_count([
        ('start', '<=', start_datetime.strftime('%Y-%m-%d %H:%M:%S')),
        ('stop', '>=', end_datetime.strftime('%Y-%m-%d %H:%M:%S'))
    ])

    if existing_appointments > 0:
        return jsonify({"error": "Ce créneau est déjà réservé. Veuillez choisir un autre horaire."}), 400

    # Création du rendez-vous
    new_appointment = appointment_model.create({
        'name': name,
        'start': start_datetime.strftime('%Y-%m-%d %H:%M:%S'),
        'stop': end_datetime.strftime('%Y-%m-%d %H:%M:%S'),
        'partner_ids': [(4, partner_id)]
    })

    if new_appointment:# Si le new rdv est validé, l'email de confirmation va être envoyé 
        formatted_date = datetime.strptime(date, "%Y-%m-%d").strftime("%d/%m/%Y")
        msg = Message("Confirmation de Rendez-vous CPM",
                      sender=app.config['MAIL_USERNAME'],
                      recipients=[email])
        msg.body = f"Bonjour {name},\n\nVotre rendez-vous au centre pédiatrique de Meyrin a été confirmé pour le {formatted_date} à {time}.\n\nEn cas d'empêchement veuillez nous prévenir 48 heures à l'avance au 022 782 54 31 (du lun-ven de 10h-16h)\nou par Fax au 022 782 59 82.\n\n\n\n\n\nCENTRE PÉDIATRIQUE MEYRIN Rue de la Prulay, 35 1217 Meyrin\nEmail:cpm@officemed.ch  Website:cpm.officemed.ch"
        mail.send(msg)
        return "success"  # Retourne un succès pour la gestion AJAX
    else:
        return "failure"  # Retourne un échec si le rendez-vous n'a pas été créé

    # Fallback pour toute autre erreur non gérée
    return jsonify({"error": "Une erreur inattendue est survenue."}), 500


@app.route('/')
def home():
    return render_template('home.html') # renvoie à la page d'acceuille lors du lancement de flask

@app.route('/new-appointment')
def new_appointment():
    return render_template('index.html')  # Le formulaire existant pour prendre un RDV

@app.route('/cancel-appointment', methods=['GET', 'POST']) # page d'annulation de rdv
def cancel_appointment():
    if request.method == 'POST':
        email = request.form['email']
        date = request.form['date']

        partner_model = odoo.env['res.partner']
        partner = partner_model.search([('email', '=', email)])
        
        if partner:
            appointment_model = odoo.env['calendar.event']
            appointments = appointment_model.search([
                ('partner_ids', '=', partner[0]),
                ('start', '>=', f"{date} 00:00:00"),
                ('start', '<=', f"{date} 23:59:59")
            ])
            if appointments:
                appointment_model.unlink(appointments)
                send_cancellation_email(email, date)  # Envoyer l'email de confirmation d'annulation
                return "success"
            else:
                return "no-appointment"
        return "no-contact"
    return render_template('cancel_appointment.html')

def send_cancellation_email(email, date):
    """Envoyer un email de confirmation d'annulation."""
    formatted_date = datetime.strptime(date, "%Y-%m-%d").strftime("%d/%m/%Y")
    msg = Message("Confirmation d'annulation de rendez-vous CPM",
                  sender=app.config['MAIL_USERNAME'],
                  recipients=[email])
    msg.body = f"Bonjour,\n\nNous vous confirmons l'annulation de votre rendez-vous prévu le {formatted_date}.\n\nCordialement,\nVotre équipe médicale"
    mail.send(msg)


if __name__ == '__main__':
    app.run(debug=True)
